cmake -S . -B build
cmake --build build
cd build
ctest
ninja test_memcheck | tee memcheck_output.txt

[ "$(tail -n 1 memcheck_output.txt)" = "Memory checking results:" ]
